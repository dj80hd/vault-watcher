# Vault-Watcher

This is a prototype/experiment in keeping tabs on paths in Vault and acting on changes to them.

## Config

See `config-example.yaml`.

## Writing a Watcher

Look at `watcher-example.yaml` for an example.
