IMAGE="registry.gitlab.com/jrr/vault-watcher"
SHA:=`git rev-parse --short HEAD`

build: lint
	go build -o bin/vault-watch

build-docker: lint
	CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o bin/vault-watch-docker

lint:
	go fmt ./...
	golint -set_exit_status ./...

test: build
	go test -timeout 1s ./...

cover: test
	goverage -covermode=set -coverprofile=cov.out `go list ./...`
	gocov convert cov.out | gocov report

coverhtml: cover
	go tool cover -html=cov.out

run: publish
	kubectl --context minikube -n vault-watcher delete pod watcher || echo "no existing pod"
	kubectl --context minikube apply -f deploy.yml
	sleep 5
	kubectl --context minikube -n vault-watcher logs -f watcher

docker: build-docker
	sudo docker build -t $(IMAGE):$(SHA) .

publish: docker
	sudo docker push $(IMAGE):$(SHA)
	sudo docker tag $(IMAGE):$(SHA) $(IMAGE):latest
	sudo docker push $(IMAGE):latest

.PHONY: build build-docker lint run docker publish
