package config

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config golint
type Config struct {
	Vault
}

// Vault golint
type Vault struct {
	CaCert string
	URL    string
	Auth
}

// Auth golint
type Auth struct {
	Path string
	Role string
}

// NewFromFile golint
func NewFromFile(filePath string) (*Config, error) {
	fileBytes, err := ioutil.ReadFile(filePath)

	if err != nil {
		return nil, err
	}

	object := Config{}

	err = yaml.Unmarshal(fileBytes, &object)

	if err != nil {
		return nil, err
	}

	return &object, nil
}
