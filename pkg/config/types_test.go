package config

import (
	"fmt"
	"reflect"
	"strings"
	"testing"
)

var goodConfig = &Config{
	Vault: Vault{
		CaCert: "CACertString",
		URL:    "URLString",
		Auth: Auth{
			Path: "PathString",
			Role: "RoleString",
		},
	},
}

var configTests = []struct {
	filename string
	config   *Config
	errstr   string
}{
	{
		"../../testdata/goodConfig.yaml",
		goodConfig,
		"",
	},
	{
		"../../testdata/emptyConfig.yaml",
		&Config{},
		"",
	},
	{
		"../../testdata/badConfig.yaml",
		nil,
		"yaml: unmarshal errors",
	},
	{
		"notafile",
		nil,
		"open notafile: no such file or directory",
	},
}

func errorContains(err error, errstr string) bool {
	if err == nil && errstr == "" {
		return true
	}
	return strings.Contains(fmt.Sprintf("%v", err), errstr)
}

func TestConfig(t *testing.T) {
	for i, tt := range configTests {
		config, err := NewFromFile(tt.filename)

		if !errorContains(err, tt.errstr) {
			t.Errorf("%d. expected error %v, to contain %v", i, err, tt.errstr)
		}

		if !reflect.DeepEqual(config, tt.config) {
			t.Errorf("%d. expected config %v, got %v", i, tt.config, config)
		}
	}
}
